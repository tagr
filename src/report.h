/* This file is part of tagr.
   Copyright (C) 2000, 2005, 2009 Max Bouglacoff, Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __report_h
#define __report_h

#define MAXADDR 64
#define MAX_NAME_LENGTH 32

typedef struct
{
  char name[MAX_NAME_LENGTH];
  unsigned long in;
  unsigned long out;
} Stat;

typedef struct
{
  int n_addr;
  unsigned long addr[1];	/* actually n_addr items */
} Stat_request;

typedef struct
{
  int n_addr;
  time_t timestamp;
  Stat stat[1];
} Stat_reply;

#define reply_size(n) (sizeof(Stat_reply) + (n-1)*sizeof(Stat))
#define request_size(n) (sizeof(Stat_request + (n-1)*sizeof(unsigned long))

void report (Stat *stat, time_t timestamp);
#endif
