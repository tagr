%{
/* This file is part of tagr.
   Copyright (C) 2000, 2005, 2006, 2009 Max Bouglacoff, Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>	
#include <tagr.h>
#include <html.gram.h>

char *html_input_file;
int html_input_line;
%}

%s EVAL

NAME [A-Za-z_][A-Za-z0-9_]*
N [0-9]+

%%
\$\$	{
	yylval.character = '$';
        return CHAR;
}
\${NAME}        {
        if (find_value (yytext+1, &yylval.value))
	  {
	    yyerror (_("unknown identifier"));
	    init_value (&yylval.value, unspecified_value, NULL);
	  }
	return IDENT;
}
\$\({NAME}\)      {
	yytext[yyleng-1] = 0;
	if (find_value (yytext+2, &yylval.value))
	  {
	    yyerror (_("unknown identifier"));
	    init_value (&yylval.value, unspecified_value, NULL);
	  }
	return IDENT;
}
\$\({NAME}:{N}\)  {
	char *p = strchr (yytext, ':');
	*p = 0;
	if (find_value (yytext+2, &yylval.value))
	  {
	    yyerror (_("unknown identifier"));
	    init_value (&yylval.value, unspecified_value, NULL);
	  }
	else
	  {
	    yylval.value.prec = strtoul (p+1, NULL, 10);
	  }
	return IDENT;
}
\$\({NAME}:[^)]+\)  {
        pp_value_t val;
        char *p = strchr (yytext, ':');
	*p++ = 0;
	if (find_value (yytext+2, &val))
	  {
	    yyerror (_("unknown identifier"));
	    init_value (&yylval.value, unspecified_value, NULL);
	  }
	else
	  {
	    size_t len = strlen (p) - 1;
	    init_value (&yylval.value, val.type, &val.v);
	    yylval.value.prec = val.prec;
	    yylval.value.format = val.format;
	    yylval.value.fmt = xmalloc (len + 1);
	    memcpy (yylval.value.fmt, p, len);
	    yylval.value.fmt[len] = 0;
	  }
	return IDENT;
}
\$\{            return OBRACE;
\$\}            return CBRACE;
<EVAL>[ \t]+    ;
<EVAL>"("|")"|"+"|"-"|"*"|"/"  return yytext[0];
<EVAL>{N}\.?([eE][-+]?{N}+)? {
         yylval.number = strtod (yytext, NULL);
	return NUMBER;
}
\n              {
	html_input_line++;
	yylval.character = yytext[0];
	return CHAR;
}
.               {
	yylval.character = yytext[0];
	return CHAR;
}

%%

void
begin_eval ()
{
  BEGIN (EVAL);
}

void
end_eval ()
{
  BEGIN (INITIAL);
}

int
html_open (char *file)
{
  yyin = fopen (file, "r");
  if (!yyin)
    {
      logmsg (L_ERR, _("cannot open input file `%s': %s"),
	     file, strerror (errno));
      return 1;
    }
  html_input_file = file;
  html_input_line = 1;
  return 0;
}

void
html_close ()
{
  if (yyin)
    {
      fclose (yyin);
      yyin = NULL;
    }
}

int
yywrap ()
{
  html_close ();
  return 1;
}
		

