/* This file is part of tagr.
   Copyright (C) 2009 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tagr.h>

#include <md5.h>

static char *msg_id;

const char *
tagr_auth_init ()
{
  asprintf (&msg_id, "<%lu.%lu@%s>",
	    (unsigned long) getpid (),
	    (unsigned long) time (NULL),
	    hostname);
  return msg_id;
}

static int
verify_apop (const char *password, const char *user_digest)
{
  int i;
  struct md5_ctx md5context;
  unsigned char md5digest[16];
  char buf[sizeof (md5digest) * 2 + 1];
  char *p;

  md5_init_ctx (&md5context);
  md5_process_bytes (msg_id, strlen (msg_id), &md5context);
  md5_process_bytes (password, strlen (password), &md5context);
  md5_finish_ctx (&md5context, md5digest);

  for (i = 0, p = buf; i < 16; i++, p += 2)
    sprintf (p, "%02x", md5digest[i]);
  return strcmp (user_digest, buf);
}

int
tagr_auth (const char *username, const char *authstr)
{
  int rc = 1;
  char *password;

  rc = udb_get_password (username, &password);
  if (rc == 1)
    {
      logmsg (L_ERR, _("no such user `%s'"), username);
    }
  else if (rc == 0)
    {
      rc = verify_apop (password, authstr);
      if (rc) 
	logmsg (L_ERR, _("authentication failed for `%s'"),
		username);
      else
	verbose (1, _("%s authenticated"), username);
      udb_free_password (password);
    }
  return rc;
}

