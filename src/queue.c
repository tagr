/* This file is part of tagr.
   Copyright (C) 2005, 2009 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <tagr.h>
     
void
queue_clear (queue_t *q)
{
  q->head = q->tail = 0;
}

int
queue_count (queue_t *q)
{
  int l;

  l = q->tail - q->head;
  if (l < 0)
    l += q->size;
  return l;
}

int
queue_is_full (queue_t *q)
{
  return queue_count(q) == q->size - 1;
}

struct traffic_history *
queue_get_ptr (queue_t *q, int index)
{
  if (q->head == q->tail || index < 0 || index >= queue_count (q))
    return NULL;
  return &q->queue[(q->head + index) % q->size];
}

struct traffic_history *
queue_get_tail (queue_t *q)
{
  int n;

  if (q->head == q->tail)
    return NULL;
  n = q->tail - 1;
  if (n < 0)
    n += q->size;
  return &q->queue[n];
}

/* Insert an entry at the tail of the queue */
void
queue_put (queue_t *q, struct traffic_history *elt)
{
  if (queue_is_full (q))
    q->head = (q->head + 1) % q->size;
  q->queue[q->tail] = *elt;
  q->tail = (q->tail + 1) % q->size;
}

/* Insert an entry at the tail of the queue.
   If the tail already contained an element, exchange it with the new one.
   Return 1 if an old element was stored in ELT, 0 otherwise. */
int
queue_xchg (queue_t *q, struct traffic_history *elt)
{
  struct traffic_history tmp;
  int rc = queue_is_full (q);

  if (rc)
    q->head = (q->head + 1) % q->size;
  q->tail %= q->size;
  tmp = q->queue[q->tail];
  q->queue[q->tail++] = *elt;
  if (rc)
    *elt = tmp;
  return rc;
}
