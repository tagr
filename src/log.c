/* This file is part of tagr.
   Copyright (C) 2000, 2005, 2009 Max Bouglacoff, Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <syslog.h>
#include <argp.h>
#include <tagr.h>

int log_facility = LOG_FACILITY;
char *log_tag;
int log_print_severity; /* FIXME: not used */

void
init_syslog (char *progname)
{
  if (!log_tag)
    log_tag = progname;
  openlog (log_tag, LOG_PID, log_facility);
}

int syslog_level[] = {
  LOG_DEBUG,
  LOG_INFO,
  LOG_NOTICE,
  LOG_WARNING,
  LOG_ERR,
  LOG_CRIT,
};

char *level_str[] = {
  N_("debug"),
  N_("info"),
  N_("notice"),
  N_("warning"),
  N_("error"),
  N_("CRITICAL"),
};


void
vlogmsg (int level, const char *fmt, va_list ap)
{
  if (grecs_log_to_stderr)
    {
      fprintf (stderr, "%s: %s: ", log_tag, gettext (level_str[level]));
      vfprintf (stderr, fmt, ap);
      fprintf (stderr, "\n");
    }
  else
    vsyslog (syslog_level[level], fmt, ap);
}

void
logmsg (int level, const char *fmt, ...)
{
  va_list ap;

  va_start (ap, fmt);
  vlogmsg (level, fmt, ap);
  va_end (ap);
}

void
die (int code, const char *fmt, ...)
{
  va_list ap;

  va_start (ap, fmt);
  vlogmsg (LOG_CRIT, fmt, ap);
  va_end (ap);
  exit (code);
}


void
verbose (int level, const char *fmt, ...)
{
  va_list ap;

  if (verbose_level < level)
    return;
  va_start (ap, fmt);
  vlogmsg (L_INFO, fmt, ap);
  va_end (ap);
}



void
grecs_print_diag (grecs_locus_t *locus, int err, int errcode, const char *msg)
{
  if (locus)
    {
      if (errcode)
	logmsg (err ? L_ERR : L_WARNING, "%s:%lu: %s: %s",
		locus->file, (unsigned long)locus->line, msg,
		strerror (errcode));
      else
	logmsg (err ? L_ERR : L_WARNING, "%s:%lu: %s",
		locus->file, (unsigned long)locus->line, msg);
    }
  else
    {
      if (errcode)
	logmsg (err ? L_ERR : L_WARNING, "%s: %s", msg,
		strerror (errcode));
      else
	logmsg (err ? L_ERR : L_WARNING, "%s", msg);
    }
}
