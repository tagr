/* This file is part of tagr.
   Copyright (C) 2009 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <wordsplit.h>
#include <tagr.h>

char *tagr_udb_name = SYSCONFDIR "/tagr.udb";

int
udb_get_password (const char *username, char **password)
{
  FILE *fp;
  size_t bufsize = 0;
  char *buf = NULL;
  unsigned line = 0;
  int rc = 1;
  
  fp = fopen (tagr_udb_name, "r");
  if (!fp)
    {
      logmsg (L_ERR, _("cannot open file %s: %s"),
	      tagr_udb_name, strerror (errno));
      return -1;
    }

  while (rc == 1 && getline (&buf, &bufsize, fp) > 0)
    {
      struct wordsplit ws;
      
      line++;
      trim_crlf (buf);
      if (wordsplit (buf, &ws, WRDSF_DEFFLAGS))
	{
	  logmsg (L_ERR, _("%s:%u: failed to parse input line"),
		  tagr_udb_name, line);
	}
      if (ws.ws_wordc == 0 || ws.ws_wordv[0][0] == '#')
	/* next */;
      else if (ws.ws_wordc < 2)
	{
	  logmsg (L_WARNING, _("%s:%u: too few words on line"),
		  tagr_udb_name, line);
	}
      else if (ws.ws_wordc > 2)
	{
	  logmsg (L_WARNING, _("%s:%u: too many words on line"),
		  tagr_udb_name, line);
	}
      else if (strcmp (ws.ws_wordv[0], username) == 0)
	{
	  *password = xstrdup (ws.ws_wordv[1]);
	  rc = 0;
	}
      wordsplit_free (&ws);
    }
	
  free (buf);
  fclose (fp);
  return rc;
}

void
udb_free_password (char *pass)
{
  char *p;
  for (p = pass; *p; p++)
    *p = 0;
  free (pass);
}
