/* This file is part of tagr.
   Copyright (C) 2006, 2009 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <ctype.h>
#include <errno.h>
#include <gd.h>
#include <gdfonts.h>

#include <tagr.h>

int fill_incoming_option = 1; /* Fill incoming graph */
int percent_option = 0;
int transparent_option = 1;
int zero_unknown_option = 1;

int color_background[3]    = { 245,245,245 };
int color_light[3]         = { 194,194,194 };
int color_dark[3]          = { 100,100,100 };
int color_major[3]         = { 255,0,0 };
int color_in[3]            = { 0,235,12 };
int color_out[3]           = { 0,94,255 };
int color_grid[3]          = { 0,0,0 };
int color_in_max[3]        = { 0,166,33 };
int color_out_max[3]       = { 255,0,255 };
int color_percent[3]       = { 239,159,79 };

int graph_xsize = 460;
int graph_ysize = 100;

int graph_h_margin[2] = { 100, 14 };
int graph_v_margin[2] = { 14, 35 };

struct monitor default_monitor = {
  NULL,
  NULL,
  NULL,
  "Bytes per Second", /* rate_unit, FIXME: I18N?? */
  0,                  /* max_rate */
  0,                  /* max_adjust */
  1.0,                /* scale */
  0.1,                /* ystep */
  0,                  /* ystep_absolute */
  0                   /* swap */
};

#define make_color_index(g, ar) \
  gdImageColorAllocate (g, (ar)[0], (ar)[1], (ar)[2])

static void
draw_vtext (gdImagePtr graph, int color, const char *text)
{
  gdImageStringUp (graph, gdFontSmall,
		   8,
		   graph_ysize + graph_v_margin[0] -
		   (graph_ysize - gdFontSmall->w * strlen (text))/2,
		   (unsigned char *) text, color);
}

static double
find_max (queue_t *dataq)
{
  int i, n = queue_count (dataq);
  double mval = 0;
  struct traffic_history *th;
  
  for (i = 0; i < n; i++)
    {
      th = queue_get_ptr (dataq, i);
      if (mval < th->inrate)
	mval = th->inrate;
      if (mval < th->outrate)
	mval = th->outrate;
    }
  return mval;
}

int
draw_graph (FILE *fp,
	    struct monitor *mon,
	    queue_t *dataq, const struct avg_acc *avg, time_t start,
	    int xstep, unsigned long xmax, 
	    int growright,
	    struct grid_class *xgrid, struct grid_class *ygrid)
{
  int x, y;
  int i, n;
  gdImagePtr graph, brush_out, brush_outm, brush_outp;
  int i_background, i_light, i_dark;
  int i_major, i_in, i_out, i_grid, i_inm, i_outm;
  int i_outp, i_outpg;
  double xscale, yscale;
  int dotted_style[3];
  int full_xsize = graph_xsize + graph_h_margin[0] + graph_h_margin[1];
  int full_ysize = graph_ysize + graph_v_margin[0] + graph_v_margin[1];
  grid_t grid;
  double ymax;

  if (mon->max_adjust)
    {
      double amax = find_max (dataq);
      if (amax > mon->max_rate)
	{
	  unsigned long n = (amax - mon->max_rate + mon->max_adjust - 1) /
	                     mon->max_adjust;
	  mon->max_rate += n * mon->max_adjust;
	}
    }
  ymax = mon->max_rate;
  
  yscale = (double) graph_ysize / ymax;
  xscale = (double) graph_xsize / xmax;
  
#define ytr(y)			    \
  (unsigned long) ((ymax >= (y) ? (ymax - (y)) : 0) * \
		    yscale + graph_h_margin[1])
#define xtr(x)			    \
  (unsigned long) (growright ?      \
		   ((full_xsize - (x)*xscale)) : \
		    (graph_h_margin[0] + (x)*xscale))

  graph = gdImageCreate (full_xsize, full_ysize);
  brush_out = gdImageCreate (1, 2);
  brush_outm = gdImageCreate (1, 2);
  brush_outp = gdImageCreate (1, 2);

  i_background = make_color_index (graph, color_background);
  i_light = make_color_index (graph, color_light);
  i_dark = make_color_index (graph, color_dark);

  if (transparent_option)
    gdImageColorTransparent (graph, i_background);

  gdImageInterlace (graph, 1);
  
  i_major = make_color_index (graph, color_major);
  i_in = make_color_index (graph, color_in);
  i_out = make_color_index (brush_out, color_out);
  i_grid = make_color_index (graph, color_grid);
  i_inm = make_color_index (graph, color_in_max);
  i_outm = make_color_index (brush_outm, color_out_max);
  i_outp = make_color_index (brush_outp, color_percent);
  i_outpg = make_color_index (graph, color_percent);

  /* Draw the image border */
  gdImageLine (graph, 0, 0, full_xsize - 1, 0, i_light);
  gdImageLine (graph, 1, 1, full_xsize - 2, 1, i_light);
  gdImageLine (graph, 0, 0, 0, full_ysize - 1, i_light);
  gdImageLine (graph, 1, 1, 1, full_ysize - 2, i_light);
  gdImageLine (graph, full_xsize - 1, 0, full_xsize - 1,
	       full_ysize - 1, i_dark);
  gdImageLine (graph, 0, full_ysize - 1, full_xsize - 1,
	       full_ysize - 1, i_dark);
  gdImageLine (graph, full_xsize - 2, 1, full_xsize - 2,
	       full_ysize - 2, i_dark);
  gdImageLine (graph, 1, full_ysize - 2, full_xsize - 2,
	       full_ysize - 2, i_dark);

  n = queue_count (dataq);

  /* Incoming traffic */
  for (i = n - 1; i > 0; i--)
    {
      struct traffic_history th, tnext;
      
      scale_sample (mon, queue_get_ptr (dataq, i), &th);
      if (th.time > start)
	continue;
      scale_sample (mon, queue_get_ptr (dataq, i - 1), &tnext);
      if (start - tnext.time > xmax)
	break;
      if (zero_unknown_option
	  && th.time - tnext.time > xstep * cut_out_fraction)
	{
	  gdImageLine (graph, xtr (start - tnext.time), ytr (0),
		       xtr (start - tnext.time), ytr (tnext.inrate), i_in);
	  th.inrate = tnext.inrate = 0;
	  th.outrate = tnext.outrate = 0;
	}
	  
      if (fill_incoming_option)
	gdImageLine (graph,
		     xtr (start - th.time), ytr (0),
		     xtr (start - th.time), ytr (tnext.inrate), i_in);
      gdImageLine (graph, xtr (start - th.time), ytr (th.inrate),
		   xtr (start - tnext.time), ytr (tnext.inrate), i_in);
    }

  /* Outgoing traffic */
  gdImageSetBrush (graph, brush_out);
  for (i = n - 1; i > 0; i--)
    {
      struct traffic_history th, tnext;
      
      scale_sample (mon, queue_get_ptr (dataq, i), &th);
      if (th.time > start)
	continue;
      scale_sample (mon, queue_get_ptr (dataq, i - 1), &tnext);
      if (start - tnext.time > xmax)
	break;

      if (zero_unknown_option
	  && th.time - tnext.time > xstep * cut_out_fraction)
	{
	  gdImageLine (graph, xtr (start - tnext.time), ytr (0),
		       xtr (start - tnext.time), ytr (tnext.inrate),
		       gdBrushed);
	  th.inrate = tnext.inrate = 0;
	  th.outrate = tnext.outrate = 0;
	}
      
      gdImageLine (graph, xtr (start - th.time), ytr (th.outrate),
		   xtr (start - tnext.time), ytr (tnext.outrate), gdBrushed);
    }

  /* Border */
  gdImageRectangle (graph,
                    xtr (0), ytr (0),
                    xtr (xmax), ytr (ymax), i_grid);


  dotted_style[0] = i_grid;
  dotted_style[1] = gdTransparent;
  dotted_style[2] = gdTransparent;
  gdImageSetStyle (graph, dotted_style, 3);

  grid = grid_create (ygrid, dataq, 0, ymax, mon);
  if (grid)
    {
      double d;
      char *str = NULL;
      
      draw_vtext (graph, i_grid, mon->rate_unit);

      /* Draw vertical grid */
      while ((d = grid_next (grid, &str, NULL)) < ymax)
	{
	  int y = ytr (d);
	  
	  /* Left tick */
	  gdImageLine (graph,
		       xtr (0) - 2, y,
		       xtr (0) + 1, y,
		       i_grid);
	  /* Right tick */
	  gdImageLine (graph,
		       xtr (xmax) - 1, y,
		       xtr (xmax) + 1, y,
		       i_grid);

	  /* Grid line */
	  gdImageLine (graph, xtr (0), y, xtr (xmax), y,
		       gdStyled);

	  if (str)
	    gdImageString (graph, gdFontSmall,
			     23, y - gdFontSmall->h / 2,
			     (unsigned char *) str, i_grid);
	}
      grid_destroy (grid);
    }
  
  grid = grid_create (xgrid, dataq, 0, xmax, &start);
  if (grid)
    {
      double d;
      char *str = NULL;
      int mark;
      
      while ((d = grid_next (grid, &str, &mark)) < xmax)
	{
	  int x = xtr (d);
	  
	  /* Tick */
	  gdImageLine (graph,
		       x, ytr (0) - 2, x, ytr (0) + 1,
		       i_grid);
	  /* Grid line */
	  gdImageLine (graph,
		       x, ytr (0), x, ytr (ymax),
		       mark ? i_major : gdStyled);

	  if (str)
	    gdImageString (graph, gdFontSmall,
			   x - strlen (str) * gdFontSmall->w / 2,
			   graph_ysize + graph_v_margin[0] + 2,
			   (unsigned char *) str, i_grid);
	}
      grid_destroy (grid);
    }
  
  /* Mark 0,0 */
  x = graph_h_margin[0];
  y = graph_ysize + graph_h_margin[1];
  gdImageLine (graph, x + 2, y + 3, x + 2, y - 3, i_major);
  gdImageLine (graph, x + 1, y + 3, x + 1, y - 3, i_major);
  gdImageLine (graph, x, y + 2, x, y - 2, i_major);
  gdImageLine (graph, x - 1, y + 1, x - 1, y - 1, i_major);
  gdImageLine (graph, x - 2, y + 1, x - 2, y - 1, i_major);
  gdImageLine (graph, x - 3, y, x - 3, y, i_major);
  
  gdImagePng (graph, fp);
  
  gdImageDestroy (graph);
  gdImageDestroy (brush_out);
  gdImageDestroy (brush_outm);
  gdImageDestroy (brush_outp);
  return 0;
}
