# Upgrade 1.0-style Tagr configuration file.
# Copyright (C) 2009 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 

NF == 0 || /[ \t]*#/ { print; next }
$1 == "user" || $1 == "template" || $1 == "basedir" {
   printf("%s;\n", $0)
   next
}
$1 == "port" {
   print "server default {
   print "    type udp;"
   print "    listen 127.0.0.1:" $2 ";"
   print "}"
   print ""
   next
}
$1 == "router" {
   print ""
   print "monitor " $2 " {"
   print "    host " $3 ";"
   print "    directory " $4 ";"
   print "    max-speed " $5/8 ";"
   print "}"
   next
}
{ print NR ": unknown statement" > "/dev/stderr" }
