# Format ipacct log file for input to tagr --read
# Copyright (C) 2009 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 

BEGIN {
    month["Jan"] = 1
    month["Feb"] = 2
    month["Mar"] = 3
    month["Apr"] = 4
    month["May"] = 5
    month["Jun"] = 6
    month["Jul"] = 7
    month["Aug"] = 8
    month["Sep"] = 9
    month["Oct"] = 10
    month["Nov"] = 11
    month["Dec"] = 12
}

# mktime input format is:
# YYYY MM DD HH MM SS
# Log entry format is:
# Sat Apr 25 00:25:00 2009   pirx    199004      4238901
#  $1  $2  $3    $4     $5    $6       $7          $8 
NF == 8 {
  gsub(/:/, " ", $4)
  print $6, mktime($5 " " month[$2] " " $3 " " $4), $8, $7
}
